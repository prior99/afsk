use std::{array::TryFromSliceError, sync::mpsc::{RecvError, SendError, TryRecvError}};
use libpulse_binding::error::PAErr;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("The target is not in the expected connection state.")]
    InvalidConnectionState,
    #[error("The channel is unknown.")]
    UnknownChannel,
    #[error("The channel encountered an error while receiving.")]
    RecvError,
    #[error("The channel encountered an error while sending.")]
    SendError,
    #[error("Failed to format a string.")]
    FormatError(#[from] std::fmt::Error),
    #[error("Input / output error.")]
    IoError(#[from] std::io::Error),
    #[error("Pulse audio error.")]
    PulseError(#[from] PAErr),
    #[error("Failed to convert slice.")]
    FromSliceError(#[from] TryFromSliceError),
    #[error("AX25 packets are expected to be at least 136 bits long. Only {0} bits received.")]
    InvalidPacketLength(usize),
    #[error("AX25 flag missing at start or end of packet.")]
    InvalidFlag,
    #[error("AX25 packet not octet aligned (length not multiple of 8).")]
    NotOctetAligned,
    #[error("AX25 packet's CRC sums didn't match.")]
    InvalidCRC,
}
impl<T> From<SendError<T>> for Error {
    fn from(_: SendError<T>) -> Self {
        Self::SendError
    }
}

impl From<TryRecvError> for Error {
    fn from(_: TryRecvError) -> Self {
        Self::RecvError
    }
}
impl From<RecvError> for Error {
    fn from(_: RecvError) -> Self {
        Self::RecvError
    }
}