use std::iter::zip;

use crate::error::Error;
use crate::algorithm::{Algorithm, ReturnFn};

/// Read an Audio Frequency Shift Keying bitstream from the output of two parallel Goertzel algorithms.
/// The first value of the input tuple is expected to be the magnitude of the **Mark**, while the second one
/// is expected to be the magnitude of the **Space**.
pub struct AFSK {
    /// Output of two parallel Goertzel algorithms. First value is magnitude for **Mark**, the other one
    /// for the **Space**.
    input: Vec<(f32, f32)>,
    /// This algorithm looks for a rising / falling edge in the input stream.
    /// This field is encoded as follows:
    /// **true**: Represents the mark.
    /// **false**: Represents the space.
    last_class: bool,

    /// The sample since the last detected edge are counted and used for determining the number
    /// of marks / flags in the current stream.
    samples_since_edge: u32,

    /// The mark frequency and the sample rate define how many samples of the input signal make up
    /// one bit of the AFSK signal.
    samples_per_mark: usize,
}

pub const MARK: bool = true;
pub const SPACE: bool = false;

impl Algorithm<(Vec<f32>, Vec<f32>), Vec<bool>> for AFSK {
    fn algorithm(
        &mut self,
        input: (Vec<f32>, Vec<f32>),
        next: &mut ReturnFn<Vec<bool>>,
    ) -> Result<(), Error> {
        let result = zip(&input.0, &input.1)
            .flat_map(|(&mark_magnitude, &space_magnitude)| {
                if let Some(result) = self.process_sample(mark_magnitude, space_magnitude) {
                    result
                } else {
                    vec![]
                }
            })
            .collect();
        next(result)?;
        Ok(())
    }
}

impl AFSK {
    pub fn new(samples_per_mark: usize) -> Self {
        AFSK {
            input: vec![],
            last_class: false,
            samples_since_edge: 0,
            samples_per_mark,
        }
    }

    pub fn process_sample(
        &mut self,
        mark_magnitude: f32,
        space_magnitude: f32,
    ) -> Option<Vec<bool>> {
        self.input.push((mark_magnitude, space_magnitude));
        let current_class = mark_magnitude > space_magnitude;

        // If the class changed, then calculate the previous streak length.
        if current_class != self.last_class {
            let num_periods =
                (self.samples_since_edge as f32 / self.samples_per_mark as f32).round() as usize;
            let result = if num_periods == 0 {
                None
            } else {
                Some(vec![self.last_class; num_periods])
            };

            // Reset counters.
            self.last_class = current_class;
            self.samples_since_edge = 1;

            result
        } else {
            self.samples_since_edge += 1;
            None
        }
    }
}
