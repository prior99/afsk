use std::fmt::Write;
use crate::error::Error;
use crate::algorithm::{Algorithm, ReturnFn};
use crate::ax25::{to_ascii, AX25};

/// Extracted from APRS specification.
#[derive(Debug, Clone)]
pub enum APRSDataType {
    AgreloDFJr,
    DoNotUse,
    InvalidData,
    Item,
    MaidenheadGridLocatorBeacon,
    MapFeature,
    Message,
    MicECurrent,
    MicEOld,
    Object,
    PeetBrosUIIWeatherStation,
    Position {
        timestamp: bool,
        aprs_messaging: bool,
    },
    Query,
    RawGPSData,
    ShelterDataWithTime,
    SpaceWeather,
    StationCapabilities,
    Status,
    TNCStreamSwitch,
    TelemetryData,
    ThirdParty,
    Unused,
    UserDefinedAPRS,
    WeatherReportWithoutPosition,
    Unknown,
}

impl From<u8> for APRSDataType {
    fn from(byte: u8) -> Self {
        match byte {
            0x1c | b'`' => APRSDataType::MicECurrent,
            b'\'' | 0x1d => APRSDataType::MicEOld,
            b'!' => APRSDataType::Position {
                timestamp: false,
                aprs_messaging: false,
            },
            b'/' => APRSDataType::Position {
                timestamp: true,
                aprs_messaging: false,
            },
            b'#' => APRSDataType::PeetBrosUIIWeatherStation,
            b'$' => APRSDataType::RawGPSData,
            b'%' => APRSDataType::AgreloDFJr,
            b'&' => APRSDataType::MapFeature,
            b')' => APRSDataType::Item,
            b'*' => APRSDataType::PeetBrosUIIWeatherStation,
            b'+' => APRSDataType::ShelterDataWithTime,
            b',' => APRSDataType::InvalidData,
            b'"' | b'(' | b'-' | b'\\' | b']' | b'^' => APRSDataType::Unused,
            b'.' => APRSDataType::SpaceWeather,
            b'0'..=b'9' | b'A'..=b'S' | b'U'..=b'Z' | b'a'..=b'z' => APRSDataType::DoNotUse,
            b':' => APRSDataType::Message,
            b';' => APRSDataType::Object,
            b'<' => APRSDataType::StationCapabilities,
            b'=' => APRSDataType::Position {
                timestamp: false,
                aprs_messaging: true,
            },
            b'>' => APRSDataType::Status,
            b'?' => APRSDataType::Query,
            b'@' => APRSDataType::Position {
                timestamp: true,
                aprs_messaging: true,
            },
            b'T' => APRSDataType::TelemetryData,
            b'[' => APRSDataType::MaidenheadGridLocatorBeacon,
            b'_' => APRSDataType::WeatherReportWithoutPosition,
            b'{' => APRSDataType::UserDefinedAPRS,
            b'|' | b'~' => APRSDataType::TNCStreamSwitch,
            b'}' => APRSDataType::ThirdParty,
            _ => APRSDataType::Unknown,
        }
    }
}

/// An APRS frame stored in an AX25 frame.
#[derive(Debug, Clone)]
pub struct APRS {
    /// APRS data type as designated by first byte in the information section
    /// of the AX25 frame.
    pub data_type: APRSDataType,
    /// Raw data from the information section of the AX25 frame.
    /// Excludes data type byte.
    pub bytes: Vec<u8>,
    /// Raw AX25 frame.
    pub frame: AX25,
}

impl APRS {
    pub fn message(&self) -> String {
        to_ascii(&self.bytes)
    }
}

impl From<&AX25> for APRS {
    fn from(frame: &AX25) -> Self {
        APRS {
            data_type: frame.info.bytes[0].into(),
            bytes: frame.info.bytes[1..].iter().cloned().collect(),
            frame: frame.clone(),
        }
    }
}

/// Continuously transform incoming AX25 frames to APRS frames.
pub struct APRSTransform {}

impl APRSTransform {
    pub fn new() -> Self {
        Self {}
    }
}

impl Algorithm<AX25, APRS> for APRSTransform {
    fn algorithm(&mut self, input: AX25, next: &mut ReturnFn<APRS>) -> Result<(), Error> {
        next(APRS::from(&input))?;
        Ok(())
    }
}

/// Will format incoming APRS frames into human-readable format and
/// yield the strings.
pub struct APRSFormat {}

impl Algorithm<APRS, String> for APRSFormat {
    fn algorithm(&mut self, aprs: APRS, next: &mut ReturnFn<String>) -> Result<(), Error> {
        let mut output = String::from("");
        writeln!(output, "APRS Packet:")?;
        writeln!(output, "    Protocol: {:?}", aprs.frame.pid)?;
        writeln!(output, "    Control: {:?}", aprs.frame.control)?;
        writeln!(output, "    Routing:")?;
        writeln!(output, "        From: {}", aprs.frame.address.source)?;
        writeln!(output, "        To: {}", aprs.frame.address.destination)?;
        writeln!(output, "        Via:")?;
        for repeater in &aprs.frame.address.repeaters {
            writeln!(output, "            - {} ({})", repeater.station, repeater.has_repeated)?;
        }
        writeln!(output, "    APRS Data Type: {:?}", aprs.data_type)?;
        writeln!(output, "    Message: \"{}\"", aprs.message())?;
        next(output)?;
        Ok(())
    }
}

impl APRSFormat {
    pub fn new() -> Self {
        APRSFormat {}
    }
}
