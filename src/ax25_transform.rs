use log::debug;

use crate::algorithm::{Algorithm, ReturnFn};
use crate::ax25::{is_flag, Modulo, AX25};
use crate::error::Error;

/// Continuously transform incoming booleans into AX25 frames. 
pub struct AX25Transform {
    /// Cache of incoming bits.
    /// Emptied once an AX25 frame has been produced.
    cache: Vec<bool>,
    /// A start flag was already found in the stream,
    /// and the next flag will indicate a full frame.
    found_start_flag: bool,
}

impl AX25Transform {
    pub fn new() -> Self {
        AX25Transform {
            cache: vec![],
            found_start_flag: false,
        }
    }
}

impl Algorithm<Vec<bool>, AX25> for AX25Transform {
    fn algorithm(&mut self, input: Vec<bool>, next: &mut ReturnFn<AX25>) -> Result<(), Error> {
        for bit in input {
            self.cache.push(bit);
            self.try_flush(next)?;
        }
        Ok(())
    }
}

impl AX25Transform {
    fn try_flush(&mut self, next: &mut ReturnFn<AX25>) -> Result<(), Error> {
        if self.cache.len() < 8 {
            return Ok(());
        }

        let tail: Vec<_> = self.cache[self.cache.len() - 8..].to_vec();
        if !is_flag(&tail) {
            return Ok(());
        }
        if !self.found_start_flag {
            self.found_start_flag = true;
            self.cache = tail;
            return Ok(());
        }

        match AX25::new(&self.cache, Modulo::Modulo8) {
            Ok(ax25) => {
                next(ax25)?;
                self.cache = vec![];
            }
            Err(Error::InvalidPacketLength(16)) => {
                self.cache = tail;
            },
            Err(err) => {
                debug!("Dropping AX25 packet: {:?}", err);
                self.cache = tail;
            }
        }
        Ok(())
    }
}
