use std::f32::consts::PI;

use crate::error::Error;
use crate::algorithm::{Algorithm, ReturnFn};

const PI2: f32 = 2f32 * PI;

/// Bandpass filter.
pub struct BandpassFilter {
    x: [f32; 2],
    y: [f32; 2],
    a: [f32; 3],
    b: [f32; 2],
}

impl BandpassFilter {
    /// Create a new bandpass filter from the following values:
    /// * `center_frequency` - Frequency of the center pole of the filter.
    /// * `bandwidth` - Width of the filter.
    /// * `sample_rate` - Sampling rate of the input signal.
    pub fn new(center_frequency: f32, bandwidth: f32, sample_rate: u32) -> Self {
        let period_length_frequency = center_frequency / (sample_rate as f32);
        let period_length_bandwidth = bandwidth / (sample_rate as f32);
        let r = 1f32 - 3f32 * period_length_bandwidth;
        let r_squared = r * r;
        let cos_frequency = f32::cos(PI2 * period_length_frequency);
        let k = (1f32 - 2f32 * r * cos_frequency + r_squared) / (2f32 - 2f32 * cos_frequency);
        BandpassFilter {
            x: [0f32; 2],
            y: [0f32; 2],
            a: [1f32 - k, 2f32 * (k - r) * cos_frequency, r_squared - k],
            b: [2f32 * r * cos_frequency, -r_squared],
        }
    }
}

impl Algorithm<Vec<f32>, Vec<f32>> for BandpassFilter {
    fn algorithm(
        &mut self,
        input: Vec<f32>,
        next: &mut ReturnFn<Vec<f32>>,
    ) -> Result<(), Error> {
        next(
            input
                .iter()
                .map(|&sample| {
                    let result = self.a[0] * sample
                        + self.a[1] * self.x[0]
                        + self.a[2] * self.x[1]
                        + self.b[0] * self.y[0]
                        + self.b[1] * self.y[1];

                    self.x = [sample, self.x[0]];
                    self.y = [result, self.y[0]];

                    result
                })
                .collect(),
        )?;
        Ok(())
    }
}
