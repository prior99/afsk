use wav::BitDepth;

use crate::error::Error;
use crate::algorithm::{Algorithm, ReturnFn};

/// An algorithm for a pipeline that reads from the output of `wav::read`.
/// Will always yield a stream of `f32`.
pub struct Wav {}

impl Algorithm<BitDepth, f32> for Wav {
    fn algorithm<'a>(&mut self, input: BitDepth, next: &mut ReturnFn<f32>) -> Result<(), Error> {
        let data = match input {
            BitDepth::Eight(data) => data
                .iter()
                .map(|&sample| (sample as f32) / (i8::MAX as f32))
                .collect::<Vec<f32>>(),
            BitDepth::Sixteen(data) => data
                .iter()
                .map(|&sample| (sample as f32) / (i16::MAX as f32))
                .collect::<Vec<f32>>(),
            BitDepth::ThirtyTwoFloat(data) => {
                data.iter().map(|&sample| sample).collect::<Vec<f32>>()
            }
            BitDepth::TwentyFour(data) => data
                .iter()
                .map(|&sample| (sample as f32) / 8_388_607f32)
                .collect::<Vec<f32>>(),
            BitDepth::Empty => {
                vec![]
            }
        };
        for sample in data {
            next(sample)?;
        }
        Ok(())
    }
}

impl Wav {
    pub fn new() -> Self {
        Wav {}
    }
}