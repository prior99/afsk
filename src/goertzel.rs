use crate::error::Error;
use crate::algorithm::{Algorithm, ReturnFn};

/// Moving window "Goertzel" algorithm.
pub struct Goertzel {
    window_size: usize,

    coeff: f32,
    cosine: f32,
    sine: f32,

    // Each entry is a pair of the Goertzel first stage sequence values - s[n-1] and s[n-2].
    // Each entry must be updated with each sample. This is a naive implementation of a sliding
    // window Goertzel filter. Its performance is O(window_size).
    filter_values: Vec<(f32, f32)>,
    yield_index: usize,
}

impl Algorithm<Vec<f32>, Vec<f32>> for Goertzel {
    fn algorithm(
        &mut self,
        input: Vec<f32>,
        next: &mut ReturnFn<Vec<f32>>,
    ) -> Result<(), Error> {
        let result = input
            .iter()
            .map(|&sample| self.process_sample(sample))
            .collect();
        next(result)?;
        Ok(())
    }
}

impl Goertzel {
    /// Instantiate a moving window Goertzel filter.
    pub fn new(sample_rate: u32, target_frequency: f32, window_size: usize) -> Self {
        let k = (window_size as f32 * target_frequency) / sample_rate as f32;
        let w = (2.0f32 * std::f32::consts::PI / window_size as f32) * k;
        let cosine = w.cos();
        let sine = w.sin();
        let coeff = 2.0f32 * cosine;

        Self {
            coeff,
            cosine,
            sine,
            window_size,
            filter_values: vec![(0f32, 0f32); window_size],
            yield_index: 0,
        }
    }

    /// Process a sample.
    /// Returns the normalized magnitude of the target frequency.
    #[inline]
    #[must_use]
    pub fn process_sample(&mut self, input: f32) -> f32 {
        for (q1, q2) in &mut self.filter_values {
            let q0 = self.coeff * *q1 - *q2 + input;
            *q2 = *q1;
            *q1 = q0;
        }

        let (q1, q2) = self.filter_values[self.yield_index];

        let real = q1 - q2 * self.cosine;
        let imag = q2 * self.sine;
        let magnitude = (real * real + imag * imag).sqrt();

        self.filter_values[self.yield_index] = (0f32, 0f32);
        self.yield_index = if self.yield_index + 1 == self.window_size {
            0
        } else {
            self.yield_index + 1
        };

        magnitude / (self.window_size as f32 / 2.0f32)
    }
}
