use crate::afsk::AFSK;
use crate::aprs::{APRSTransform, APRS};
use crate::ax25::AX25;
use crate::ax25_transform::AX25Transform;
use crate::bandpass_filter::BandpassFilter;
use crate::error::Error;
use crate::goertzel::Goertzel;
use crate::nrzi::NRZI;
use crate::pipeline::{Node, Pipeline};
use crate::{parallel, sequence, step};

/// Arbitrary AFSK pipeline:
/// * Bandpass filter around the center between mark and space frequency.
/// * Two parallel goertzel algorithms for detecting mark and space.
/// * AFSK demodulator.
pub fn afsk(
    sample_rate: u32,
    mark_fq: f32,
    space_fq: f32,
) -> Result<Pipeline<Vec<f32>, Vec<bool>>, Error> {
    let samples_per_mark = (sample_rate as f32 / mark_fq).round() as usize;
    // let center_frequency = (mark_fq - space_fq).abs() / 2f32 + f32::min(mark_fq, space_fq);
    Ok(sequence!(
        step!(BandpassFilter::new(1_700f32, 1000f32, sample_rate)),
        parallel!(
            step!(Goertzel::new(sample_rate, space_fq, samples_per_mark)),
            step!(Goertzel::new(sample_rate, mark_fq, samples_per_mark))
        ),
        step!(AFSK::new(samples_per_mark))
    ))
}

/// This frequency also defines the period size.
/// The baud is given as `sample_rate / BELL_202_MARK_FQ`.
const BELL_202_MARK_FQ: f32 = 1_200f32; // Low frequency, stands for a binary 1.
const BELL_202_SPACE_FQ: f32 = 2_200f32; // High frequency, but usually valued 0.

/// Specialized version of an AFSK demodulator for Bell 202 standard.
pub fn bell_202(sample_rate: u32) -> Result<Pipeline<Vec<f32>, Vec<bool>>, Error> {
    afsk(sample_rate, BELL_202_MARK_FQ, BELL_202_SPACE_FQ)
}

/// AX25 pipeline:
/// * NRZI
/// * AX25
pub fn ax25() -> Result<Pipeline<Vec<bool>, AX25>, Error> {
    Ok(sequence!(step!(NRZI::new()), step!(AX25Transform::new())))
}

/// APRS pipeline:
/// * NRZI
/// * AX25
/// * APRS
pub fn aprs() -> Result<Pipeline<Vec<bool>, APRS>, Error> {
    Ok(sequence!(ax25()?, step!(APRSTransform::new())))
}
