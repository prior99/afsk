pub mod afsk;
pub mod algorithm;
pub mod aprs;
pub mod ax25;
pub mod ax25_transform;
pub mod bandpass_filter;
pub mod error;
pub mod goertzel;
pub mod nrzi;
pub mod pipeline;
pub mod pipelines;
pub mod wav_utils;

use std::fs::File;
use std::path::PathBuf;

use clap::{Parser, Subcommand};
use libpulse_binding::sample::{Format, Spec};
use libpulse_binding::stream::Direction;
use libpulse_simple_binding::Simple;
use log::{info, Level};

use crate::algorithm::Chunk;
use crate::aprs::APRSFormat;
use crate::error::Error;
use crate::pipeline::Node;
use crate::pipelines::{aprs, bell_202};
use crate::wav_utils::Wav;

#[derive(Debug, Parser)]
#[command(name = "radio")]
#[command(about = "Parse APRS packets from audio sources.", long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Read from an audio (wav) file.
    Read {
        #[arg(required = true)]
        path: PathBuf,
        #[arg(short, long, default_value_t = false)]
        verbose: bool,
    },
    /// Record audio from pulseaudio.
    Record {
        #[arg(short, long, default_value_t = false)]
        verbose: bool
    },
}

fn read(path: PathBuf) -> Result<(), Error> {
    let mut file = File::open(path)?;
    let (header, data) = wav::read(&mut file)?;
    let mut pipeline = sequence!(
        step!(Wav::new()),
        step!(Chunk::new(512)),
        bell_202(header.sampling_rate)?,
        aprs()?,
        step!(APRSFormat::new())
    );
    pipeline.execute(&data)?;
    while let Some(output) = pipeline.try_recv()? {
        info!("{}", output);
    }
    Ok(())
}

fn record() -> Result<(), Error> {
    let sample_rate = 44100;

    let mut pipeline = sequence!(bell_202(sample_rate)?, aprs()?, step!(APRSFormat::new()));

    let spec = Spec {
        format: Format::FLOAT32NE,
        channels: 1,
        rate: sample_rate,
    };

    let pulse = Simple::new(
        None,
        "Radio",
        Direction::Record,
        None,
        "Radio recording for APRS.",
        &spec,
        None,
        None,
    )?;

    let mut data = [0u8; 512];

    loop {
        pulse.read(&mut data)?;
        let samples: Vec<f32> = data
            .chunks(4)
            .map(|chunk| f32::from_ne_bytes(chunk.try_into().unwrap()))
            .collect();
        pipeline.execute(&samples)?;

        while let Some(output) = pipeline.try_recv()? {
            info!("{}", output);
        }
    }
}

fn main() -> Result<(), Error> {
    let args = Cli::parse();

    match args.command {
        Commands::Read { path, verbose } => {
            let level = if verbose { Level::Debug } else { Level::Info };
            simple_logger::init_with_level(level).unwrap();
            read(path)
        },
        Commands::Record { verbose } => {
            let level = if verbose { Level::Debug } else { Level::Info };
            simple_logger::init_with_level(level).unwrap();
            record()
        }
    }
}
