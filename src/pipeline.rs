use std::sync::mpsc::{channel, Receiver, Sender, TryRecvError};

use crate::{algorithm::Algorithm, error::Error};

/// Converts a `Result<T, TryRecvError>` to `Result<Option<T>, Error>`.
pub fn from_try_recv_result<T>(value: Result<T, TryRecvError>) -> Result<Option<T>, Error> {
    match value {
        Ok(value) => Ok(Some(value)),
        Err(TryRecvError::Empty) => Ok(None),
        Err(err) => Err(err.into()),
    }
}

/// Represents a node in the pipeline.
pub trait Node<I: Clone, O>: Input<I> + Output<O> {
    /// Perform operations (like executing the algorithm).
    /// Must return `true` if work has been performed and `false` otherwise.
    fn tick(&mut self) -> Result<bool, Error>;

    /// Insert input data into the node's input buffer.
    /// Will not trigger computations.
    fn send(&mut self, data: &I) -> Result<(), Error> {
        Ok(self.input().send(data.clone())?)
    }

    /// Blocking receive operation for the node's output data.
    fn recv(&mut self) -> Result<O, Error> {
        self.output().recv().map_err(Into::<Error>::into)
    }

    /// Non-blocking receive operation for the node's output data.
    fn try_recv(&mut self) -> Result<Option<O>, Error> {
        from_try_recv_result(self.output().try_recv())
    }

    /// Insert the input data and tick the node (e.g. execute the algorithm)
    /// until no more work has been done while ticking.
    fn execute(&mut self, data: &I) -> Result<(), Error> {
        self.send(data)?;
        while self.tick()? {};
        Ok(())
    }
}

/// A transform node that takes an algorithm as an argument and uses it
/// in order to transform the input data into the output data.
pub struct Transform<'a, I, O> {
    input: (Sender<I>, Receiver<I>),
    output: (Sender<O>, Receiver<O>),
    pub algorithm: Box<dyn Algorithm<I, O> + 'a>,
}

impl<'a, I: Clone, O> Transform<'a, I, O> {
    pub fn new(processor: impl Algorithm<I, O> + 'a) -> Self {
        Self {
            input: channel(),
            output: channel(),
            algorithm: Box::new(processor),
        }
    }
}

impl<'a, I: Clone, O> Node<I, O> for Transform<'a, I, O> {
    fn tick(&mut self) -> Result<bool, Error> {
        match self.input.1.try_recv() {
            Ok(data) => {
                let output = &mut self.output.0;
                let mut closure = |data| Ok(output.send(data)?);
                (*self.algorithm).algorithm(data, &mut closure)?;
                Ok(true)
            }
            Err(TryRecvError::Empty) => Ok(false),
            Err(err) => Err(err.into()),
        }
    }
}

impl<'a, I: Clone, O> Output<O> for Transform<'a, I, O> {
    fn output(&mut self) -> &mut Receiver<O> {
        &mut self.output.1
    }
}

impl<'a, I: Clone, O> Input<I> for Transform<'a, I, O> {
    fn input(&mut self) -> &mut Sender<I> {
        &mut self.input.0
    }
}

/// A wrapper around an output receiver.
pub trait Output<T> {
    fn output(&mut self) -> &mut Receiver<T>;
}

/// A wrapper around an input receiver.
pub trait Input<T> {
    fn input(&mut self) -> &mut Sender<T>;
}

/// Ticking function closure.
pub type TickFn = dyn FnMut() -> Result<bool, Error>;

/// A node that wraps other nodes.
pub struct Pipeline<I, O> {
    tick_pipeline: Box<TickFn>,
    tx_in: Sender<I>,
    rx_out: Receiver<O>,
}

impl<I: Clone, O> Pipeline<I, O> {
    pub fn new(tick_pipeline: Box<TickFn>, tx_in: Sender<I>, rx_out: Receiver<O>) -> Self {
        Self {
            tick_pipeline,
            tx_in,
            rx_out,
        }
    }
}

impl<I: Clone, O> Node<I, O> for Pipeline<I, O> {
    fn tick(&mut self) -> Result<bool, Error> {
        (self.tick_pipeline)()
    }
}

impl<I: Clone, O> Output<O> for Pipeline<I, O> {
    fn output(&mut self) -> &mut Receiver<O> {
        &mut self.rx_out
    }
}

impl<I: Clone, O> Input<I> for Pipeline<I, O> {
    fn input(&mut self) -> &mut Sender<I> {
        &mut self.tx_in
    }
}

#[macro_export]
macro_rules! recursify {
    (($last:expr)) => {
        ($last, ())
    };
    (($last:expr $(, $node:expr)* $(,)?)) => {
        ($last, $crate::recursify!(( $($node),* )))
    };
}

#[macro_export]
macro_rules! sequence {
    (@wrap $tx_out:expr, $data:expr, $nodes:expr, $last:expr) => {
        $nodes.0.execute(&$data)?;
        while let Some(data) = $nodes.0.try_recv()? {
            $tx_out.send(data)?;
        }
    };
    (@wrap $tx_out:expr, $data:expr, $nodes:expr, $last:expr $(, $node:expr)*) => {
        $nodes.0.execute(&$data)?;
        while let Some(data) = $nodes.0.try_recv()? {
            let nodes = &mut $nodes.1;
            $crate::sequence!(@wrap $tx_out, data, nodes, $($node),*);
        }
    };
    ($($node:expr),*  $(,)? ) => {{
        use std::sync::mpsc::channel;
        use $crate::pipeline::{Pipeline, from_try_recv_result};
        use $crate::error::Error;

        let (tx_in, rx_in) = channel();
        let (tx_out, rx_out) = channel();

        let mut nodes = $crate::recursify!(($($node),*));

        let tick = move || -> Result<bool, Error> {
            while let Some(data) = from_try_recv_result(rx_in.try_recv())? {
                let nodes = &mut nodes;
                $crate::sequence!(@wrap tx_out, data, nodes, $($node),*);
            }
            Ok(false)
        };

        Pipeline::new(Box::new(tick), tx_in, rx_out)
    }};
}

#[macro_export]
macro_rules! step {
    ($node:expr) => {
        $crate::pipeline::Transform::new($node)
    };
}

#[macro_export]
macro_rules! parallel {
    (@send_and_tick $nodes_send:expr, $cache_send:expr, $input:expr, $last:expr) => {
        $nodes_send.0.execute(&$input)?;
        if let Some(value) = (&mut $nodes_send.0).try_recv()? {
            $cache_send.0.push_back(value);
        };
    };
    (@send_and_tick $nodes_send:expr, $cache_send:expr, $input:expr, $last:expr $(,$node:expr)*) => {
        $nodes_send.0.execute(&$input)?;
        if let Some(value) = (&mut $nodes_send.0).try_recv()? {
            $cache_send.0.push_back(value);
        };
        let nodes_send = &mut $nodes_send.1;
        let cache_send = &mut $cache_send.1;
        parallel!(@send_and_tick nodes_send, cache_send, $input, $($node),*);
    };
    (@cache_literal $last:expr) => {
        (VecDeque::new(), ())
    };
    (@cache_literal $last:expr $(,$node:expr)*) => {
        (VecDeque::new(), parallel!(@cache_literal $($node),*))
    };
    (@cache_empty $cache:expr, $last:expr) => {
        !$cache.0.is_empty()
    };
    (@cache_empty $cache:expr, $last:expr $(,$node:expr)*) => {
        !$cache.0.is_empty() && parallel!(@cache_empty $cache.1, $($node),*)
    };
    (@output $cache:expr, $nodes:tt) => { $nodes };
    (@output $cache:expr, ( $($nodes:tt)* ) $last:expr) => {
        parallel!(@output $cache, ( $($nodes)* $cache.0.pop_front().unwrap() ))
    };
    (@output $cache:expr, ( $($nodes:tt)* ) $last:expr, $($data:expr),*) => {
        parallel!(@output $cache.1, ( $($nodes)* $cache.0.pop_front().unwrap(), ) $($data),*)
    };

    ($($node:expr),*  $(,)? ) => {{
        use std::{collections::VecDeque, sync::mpsc::channel};
        use $crate::pipeline::{Pipeline, from_try_recv_result};
        use $crate::error::Error;

        let mut nodes = $crate::recursify!(($($node),*));

        let mut cache = (parallel!(@cache_literal $($node),*));

        let (tx_in, rx_in) = channel();
        let (tx_out, rx_out) = channel();

        let tick = move || -> Result<bool, Error> {
            while let Some(input) = from_try_recv_result(rx_in.try_recv())? {
                let nodes_send = &mut nodes;
                let cache_send = &mut cache;
                parallel!(@send_and_tick nodes_send, cache_send, input, $($node),*);

                while parallel!(@cache_empty cache, $($node),*) {
                    tx_out.send(parallel!(@output cache, () $($node),*))?;
                }
            }

            Ok(false)
        };

        Pipeline::new(Box::new(tick), tx_in, rx_out)
    }}
}
