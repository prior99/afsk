use std::{collections::VecDeque, fmt::Display};

use crc::Crc;
use crc::CRC_16_IBM_SDLC;

use crate::error::Error;

/// Represents an amateur radio station.
#[derive(Debug, PartialEq, Clone)]
pub struct Station {
    /// Callsign of the station.
    pub callsign: String,
    /// 5 bit SSID (0 .. 16) uniquely identifying this station.
    pub ssid: u8,
}

impl Station {
    pub fn new(callsign: String, ssid: u8) -> Self {
        Station { callsign, ssid }
    }
}

impl From<&[u8]> for Station {
    fn from(bytes: &[u8]) -> Self {
        let callsign = &bytes[0..6]
            .iter()
            .map(|byte| byte >> 1)
            .collect::<Vec<u8>>();
        let callsign = to_ascii(callsign).trim_end().into();
        let ssid = (bytes[6] & 0b00011110) >> 1;
        Station::new(callsign, ssid)
    }
}

impl Display for Station {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}-{}", self.callsign, self.ssid)
    }
}

/// A repeater in the trace of an AX25 frame.
#[derive(Debug, PartialEq, Clone)]
pub struct Repeater {
    /// Station identification.
    pub station: Station,
    /// Whether the repeater has repeated the packet already, or not.
    pub has_repeated: bool,
}

impl Repeater {
    pub fn new(station: Station, has_repeated: bool) -> Self {
        Repeater {
            station,
            has_repeated,
        }
    }
}

impl From<&[u8]> for Repeater {
    fn from(bytes: &[u8]) -> Self {
        Repeater::new(bytes.into(), (bytes[6] & 0b00000001) == 1)
    }
}

/// "Direction" of the frame.
/// Commands are sent from a station to another station independently.
/// Responses are a reaction of a station to a previous command.
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Direction {
    Command,
    Response,
    Unknown,
}

impl From<&[u8]> for Direction {
    fn from(bytes: &[u8]) -> Self {
        let ssid_c_bit_destination = (bytes[6] & 0b10000000) != 0;
        let ssid_c_bit_source = (bytes[13] & 0b10000000) != 0;
        match (ssid_c_bit_destination, ssid_c_bit_source) {
            (true, false) => Direction::Command,
            (false, true) => Direction::Response,
            _ => Direction::Unknown,
        }
    }
}

/// Address header of an AX25 frame. Includes the source and destination of the frame,
/// as well as all participating repeaters and the direction.
#[derive(Debug, PartialEq, Clone)]
pub struct Address {
    /// This station originally issued the message.
    pub source: Station,
    /// This is the destination station for whom the message is intended.
    pub destination: Station,
    pub direction: Direction,
    /// List of all participating repeaters in the message's trace.
    pub repeaters: Vec<Repeater>,
}

impl Address {
    pub fn from_bytes(data: &[u8]) -> Self {
        let destination = data[0..8].into();
        let source = data[7..14].into();
        let direction = data.into();
        let repeaters = Self::read_repeaters(&data[14..]);

        Address {
            destination,
            source,
            direction,
            repeaters,
        }
    }

    fn read_repeaters(mut data: &[u8]) -> Vec<Repeater> {
        let mut result = vec![];
        while data.len() >= 8 && (data[7] & 0b10000000) != 0 {
            result.push(data.into());
            data = &data[7..];
        }
        result
    }
}

/// Convert a byte to ASCII without asking questions.
pub fn to_ascii(data: &[u8]) -> String {
    String::from_iter(data.iter().map(|b| *b as char))
}

/// Control section of a supervisory frame.
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum SupervisoryControl {
    ReceiveReady,
    ReceiveNotReady,
    Reject,
    SelectiveReject,
}

impl From<&[u8]> for SupervisoryControl {
    fn from(bytes: &[u8]) -> Self {
        match (bytes[0] & 0b00001100) >> 2 {
            0b00 => SupervisoryControl::ReceiveReady,
            0b01 => SupervisoryControl::ReceiveNotReady,
            0b10 => SupervisoryControl::Reject,
            0b11 => SupervisoryControl::SelectiveReject,
            _ => unreachable!(),
        }
    }
}

/// Control section of an unnumbered frame.
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum UnnumberedControl {
    SetAsyncBalancedModeExtended,
    SetAsyncBalancedMode,
    Disconnect,
    DisconnectMode,
    UnnumberedAcknowledge,
    FrameReject,
    UnnumberedInformation,
    ExchangeInformation,
    Test,
}

impl From<&[u8]> for UnnumberedControl {
    fn from(bytes: &[u8]) -> Self {
        match bytes[0] & 0b11101100 {
            0b01101100 => UnnumberedControl::SetAsyncBalancedModeExtended,
            0b00101100 => UnnumberedControl::SetAsyncBalancedMode,
            0b01000000 => UnnumberedControl::Disconnect,
            0b00001100 => UnnumberedControl::DisconnectMode,
            0b01100000 => UnnumberedControl::UnnumberedAcknowledge,
            0b10000100 => UnnumberedControl::FrameReject,
            0b00000000 => UnnumberedControl::UnnumberedInformation,
            0b10101100 => UnnumberedControl::ExchangeInformation,
            0b11100000 => UnnumberedControl::Test,
            _ => unreachable!(),
        }
    }
}

/// Control section of a packet.
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Control {
    Information {
        send_sequence_number: u8,
        receive_sequence_number: u8,
        poll_final: bool,
    },
    Supervisory {
        receive_sequence_number: u8,
        poll_final: bool,
        control: SupervisoryControl,
    },
    Unnumbered {
        poll_final: bool,
        control: UnnumberedControl,
    },
}

impl Control {
    pub fn from_bytes(bytes: &[u8]) -> Self {
        let poll_final = Self::read_poll_final(&bytes);
        match bytes[0] & 0b00000011 {
            0b01 => Control::Supervisory {
                poll_final,
                receive_sequence_number: Self::read_receive_sequence_number(&bytes),
                control: bytes.into(),
            },
            0b11 => Control::Unnumbered {
                poll_final,
                control: bytes.into(),
            },
            _ => Control::Information {
                receive_sequence_number: Self::read_receive_sequence_number(&bytes),
                send_sequence_number: Self::read_send_sequence_number(&bytes),
                poll_final,
            },
        }
    }

    fn read_poll_final(bytes: &[u8]) -> bool {
        match bytes.len() {
            1 => (bytes[0] & 0b00010000) != 0,
            _ => (bytes[1] & 0b00000001) != 0,
        }
    }

    fn read_receive_sequence_number(bytes: &[u8]) -> u8 {
        match bytes.len() {
            1 => (bytes[0] & 0b11100000) >> 5,
            _ => (bytes[1] & 0b11111110) >> 1,
        }
    }

    fn read_send_sequence_number(bytes: &[u8]) -> u8 {
        match bytes.len() {
            1 => (bytes[0] & 0b00001110) >> 1,
            _ => (bytes[0] & 0b11111110) >> 1,
        }
    }
}

/// Used protocol for this frame.
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum ProtocolId {
    Iso8208,
    TCPCompressed,
    TCPUncompressed,
    SegmentationFragment,
    TexNetDatagramProtocol,
    LinkQualityProtocol,
    Appletalk,
    AppletalkArp,
    ArpaInternetProtocol,
    ArpaAddressResolution,
    FlexNet,
    NetRom,
    NothingImplemented,
    Other(u8),
    Unknown,
}

impl From<&[u8]> for ProtocolId {
    fn from(bytes: &[u8]) -> Self {
        match bytes[0] {
            0b00000001 => ProtocolId::Iso8208,
            0b00000110 => ProtocolId::TCPCompressed,
            0b00000111 => ProtocolId::TCPUncompressed,
            0b00001000 => ProtocolId::SegmentationFragment,
            0b11000011 => ProtocolId::TexNetDatagramProtocol,
            0b11000100 => ProtocolId::LinkQualityProtocol,
            0b11001010 => ProtocolId::Appletalk,
            0b11001011 => ProtocolId::AppletalkArp,
            0b11001100 => ProtocolId::ArpaInternetProtocol,
            0b11001101 => ProtocolId::ArpaAddressResolution,
            0b11001110 => ProtocolId::FlexNet,
            0b11001111 => ProtocolId::NetRom,
            0b11110000 => ProtocolId::NothingImplemented,
            0b11111111 => ProtocolId::Other(bytes[1]),
            _ => ProtocolId::Unknown,
        }
    }
}

/// Information section of the frame.
#[derive(Debug, PartialEq, Clone)]
pub struct Information {
    pub bytes: Vec<u8>,
}

impl Information {
    pub fn new(bytes: &[u8]) -> Self {
        Information {
            bytes: bytes.to_vec(),
        }
    }

    pub fn as_string(&self) -> String {
        String::from_iter(self.bytes.iter().map(|b| *b as char))
    }
}

/// Each AX25 network must agree on a "modulo" of 8 bit or 128 bit.
/// This is used for the control section of the frame. 128 bit allow
/// longer sequence numbers.
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Modulo {
    Modulo8,
    Modulo128,
}

/// An AX25 frame.
#[derive(Debug, PartialEq, Clone)]
pub struct AX25 {
    pub address: Address,
    pub control: Control,
    pub pid: Option<ProtocolId>,
    pub info: Information,
}

impl AX25 {
    pub fn new(bits: &[bool], modulo: Modulo) -> Result<AX25, Error> {
        // Frames with less than 136 bits are always invalid.
        if bits.len() < 136 {
            return Err(Error::InvalidPacketLength(bits.len()));
        }

        // The start and the end flag must be present and valid.
        if &bits[0..8] != &[false, true, true, true, true, true, true, false] {
            return Err(Error::InvalidFlag);
        }
        if &bits[bits.len() - 8..] != &[false, true, true, true, true, true, true, false] {
            return Err(Error::InvalidFlag);
        }

        // Remove the flags from the frame.
        let bits = &bits[8..bits.len() - 8];

        // Everything from here on is bit stuffed.
        // The encoder will insert a zero after every sequence of 6 bits.
        // The stuffing `0` bit must be removed.
        let bits = unstuff(bits);

        // The message must be octet aligned (dividable by eight).
        if (bits.len() % 8) != 0 {
            return Err(Error::NotOctetAligned);
        }

        // Convert bits to u8 bytes.
        let mut bytes = bits_to_bytes(&bits);

        // Read and compare the CRC checksum of the frame with a locally calculated one.
        let remote_crc: u16 = ((bytes.pop().unwrap() as u16) << 8) | (bytes.pop().unwrap() as u16);
        let local_crc = Crc::<u16>::new(&CRC_16_IBM_SDLC).checksum(&bytes);
        if remote_crc != local_crc {
            return Err(Error::InvalidCRC);
        }

        // Store the bytes in a double ended queue, so that they can be read from the front.
        let mut bytes = bytes.iter().cloned().collect::<VecDeque<_>>();

        // Decode address. While the last bit of each byte is `0`, the byte belongs to the address field.
        let mut address_bytes = vec![];
        while let Some(byte) = bytes.pop_front() {
            address_bytes.push(byte);
            if (byte & 0b00000001) == 1 {
                break;
            }
        }
        let address = Address::from_bytes(&address_bytes);

        // Decode the control portion of the frame. The length is known from the module type of the network.
        let control: Vec<u8> = match modulo {
            Modulo::Modulo8 => vec![bytes.pop_front().unwrap()],
            Modulo::Modulo128 => vec![bytes.pop_front().unwrap(), bytes.pop_front().unwrap()],
        };
        let control = Control::from_bytes(control.as_slice());

        // Read the protocol identifier from the frame. This can either be one or two bytes, depending on the
        // first byte.
        let pid = match &control {
            Control::Information { .. }
            | Control::Unnumbered {
                control: UnnumberedControl::UnnumberedInformation,
                ..
            } => {
                let byte = bytes.pop_front().unwrap();
                let pid: ProtocolId = vec![byte, bytes[0]].as_slice().into();
                if let ProtocolId::Other(_) = pid {
                    bytes.pop_front();
                }
                Some(pid)
            }
            _ => None,
        };

        // With the headers all consumed and the CRC checksum removed from the frame, the remaining bytes 
        // of the frame are now the payload (info) section.
        let bytes = bytes.make_contiguous();
        let info = Information::new(bytes);

        // Return the completed frame.
        Ok(AX25 {
            address,
            control,
            pid,
            info,
        })
    }
}

/// Used for removing the stuffing bits from a frame.
/// During the encoding process, a `0` is inserted after every sequence of five `1`.
/// This `0` must be removed. 
fn unstuff(mut bits: &[bool]) -> Vec<bool> {
    let mut result = vec![];

    while !bits.is_empty() {
        if bits.len() >= 6 && &bits[0..6] == &[true, true, true, true, true, false] {
            result.extend(&[true; 5]);
            bits = &bits[6..]
        } else {
            result.push(bits[0]);
            bits = &bits[1..];
        }
    }

    result
}

/// Convert a sequence of bits to bytes.
pub fn bits_to_bytes(bits: &[bool]) -> Vec<u8> {
    let mut result = vec![];

    let iter = bits.chunks(8);

    for byte in iter {
        result.push(bits_to_byte(byte));
    }

    result
}

/// Convert a sequence of bits to one byte in LE order.
pub fn bits_to_byte(bits: &[bool]) -> u8 {
    let mut value = 0;
    for (index, bit) in bits.iter().enumerate() {
        let bit = if *bit { 1 } else { 0 };
        value = value | (bit << index);
    }
    value
}

/// Matches a flag (`01111110`).
pub fn is_flag(bits: &[bool]) -> bool {
    if bits.len() != 8 {
        return false;
    }
    return &bits == &[false, true, true, true, true, true, true, false];
}

/// Finds the index of the first bit of a flag (`01111110`) in a bitstream.
pub fn find_next_flag(mut bits: &[bool]) -> Option<usize> {
    let mut index = 0;
    while bits.len() >= 8 {
        if &bits[0..8] == &[false, true, true, true, true, true, true, false] {
            return Some(index);
        }
        index += 1;
        bits = &bits[1..];
    }

    None
}

/// Parse a chunk of bits into a list of AX25 frames.
pub fn parse_all(decoded: &[bool]) -> Vec<AX25> {
    let mut flag_start = find_next_flag(&decoded).unwrap();

    let mut result = vec![];
    while let Some(next_flag) = find_next_flag(&decoded[flag_start + 1..]) {
        let flag_end = flag_start + next_flag + 1;

        let frame = &decoded[flag_start..flag_end + 8];

        if frame.len() > 16 {
            if let Ok(ax25) = AX25::new(frame, Modulo::Modulo8) {
                result.push(ax25)
            }
        }

        flag_start = flag_end;
    }

    result
}
