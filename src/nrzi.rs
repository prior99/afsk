use crate::error::Error;
use crate::algorithm::{Algorithm, ReturnFn};


/// Non-Return-To-Zero-Inverted algorithm.
/// Decodes a sequence of bits from a sequence of NRZI encoded bits.
pub struct NRZI {
    last_bit: bool,
}

impl Algorithm<Vec<bool>, Vec<bool>> for NRZI {
    fn algorithm(
        &mut self,
        input: Vec<bool>,
        next: &mut ReturnFn<Vec<bool>>,
    ) -> Result<(), Error> {
        let result = input
            .iter()
            .map(|&bit| {
                let result = bit == self.last_bit;
                self.last_bit = bit;
                result
            })
            .collect();
        next(result)?;
        Ok(())
    }
}

impl NRZI {
    pub fn new() -> Self {
        NRZI { last_bit: false }
    }
}
