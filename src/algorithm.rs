use std::fmt::Debug;
use std::mem::replace;

use crate::error::Error;

/// Used for returning a value from an algorithm.
pub type ReturnFn<'a, T> = dyn FnMut(T) -> Result<(), Error> + 'a;

/// Represents an executable algorithm.
pub trait Algorithm<I, O> {
    /// Execute the algorithm. The argument `next` may be called multiple times or not at all
    /// for yielding a value from the algorithm.
    fn algorithm<'a>(&mut self, input: I, next: &mut ReturnFn<O>) -> Result<(), Error>;
}

/// Calculate the length of a string.
pub struct StringLength {}

impl StringLength {
    pub fn new() -> Self {
        StringLength {}
    }
}

impl Algorithm<String, u32> for StringLength {
    fn algorithm(&mut self, input: String, next: &mut ReturnFn<u32>) -> Result<(), Error> {
        next(input.len() as u32)?;
        Ok(())
    }
}

/// Multiply the input with a specified factor.
pub struct Multiply {
    factor: u32,
}

impl Multiply {
    pub fn new(factor: u32) -> Self {
        Multiply { factor }
    }
}

impl Algorithm<u32, u32> for Multiply {
    fn algorithm(&mut self, input: u32, next: &mut ReturnFn<u32>) -> Result<(), Error> {
        next(input * self.factor)?;
        Ok(())
    }
}

/// Sum the contents of a vector.
pub struct Sum {}

impl Sum {
    pub fn new() -> Self {
        Sum {}
    }
}

impl Algorithm<Vec<u32>, u32> for Sum {
    fn algorithm(&mut self, input: Vec<u32>, next: &mut ReturnFn<u32>) -> Result<(), Error> {
        next(input.iter().sum())?;
        Ok(())
    }
}

/// Buffer the input values in a buffer and flush it once the buffer is full.
pub struct Chunk<T> {
    buffer: Vec<T>,
    buffer_length: usize,
}

impl<T> Chunk<T> {
    pub fn new(buffer_length: usize) -> Self {
        Chunk {
            buffer: vec![],
            buffer_length,
        }
    }
}

impl<T> Algorithm<T, Vec<T>> for Chunk<T> {
    fn algorithm(&mut self, input: T, next: &mut ReturnFn<Vec<T>>) -> Result<(), Error> {
        self.buffer.push(input);
        if self.buffer.len() >= self.buffer_length {
            let buffer = replace(&mut self.buffer, vec![]);
            next(buffer)?;
        }
        Ok(())
    }
}

/// Convert input value to string.
pub struct Stringify {}

impl Stringify {
    pub fn new() -> Self {
        Self {}
    }
}

impl Algorithm<u32, String> for Stringify {
    fn algorithm<'a>(&mut self, input: u32, next: &mut ReturnFn<String>) -> Result<(), Error> {
        next(input.to_string())
    }
}

/// Add a value to every input value.
pub struct Add {
    value: u32,
}

impl Add {
    pub fn new(value: u32) -> Self {
        Add { value }
    }
}

impl Algorithm<u32, u32> for Add {
    fn algorithm(&mut self, input: u32, next: &mut ReturnFn<u32>) -> Result<(), Error> {
        next(input + self.value)?;
        Ok(())
    }
}

/// Print the input data and forward it.
pub struct Printf {
    tag: String,
}

impl Printf {
    pub fn new(tag: &str) -> Self {
        Printf { tag: tag.into() }
    }
}

impl<T> Algorithm<T, T> for Printf
where
    T: Debug,
{
    fn algorithm(&mut self, input: T, next: &mut ReturnFn<T>) -> Result<(), Error> {
        println!("{}: {:?}", self.tag, input);
        next(input)?;
        Ok(())
    }
}
