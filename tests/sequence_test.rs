use radio::algorithm::{Chunk, Multiply, StringLength, Sum};
use radio::{sequence, step};
use radio::pipeline::Node;

#[test]
fn test_sequence() {
    let mut pipeline = sequence!(
        step!(StringLength::new()),
        step!(Multiply::new(2)),
        step!(Multiply::new(3)),
    );

    pipeline.send(&String::from("12345")).unwrap();
    pipeline.tick().unwrap();
    assert_eq!(pipeline.try_recv().unwrap(), Some(30));
}

#[test]
fn test_chains() {
    let pipeline1 = sequence!(step!(StringLength::new()));

    let pipeline2 = sequence!(
        step!(Multiply::new(2)),
        step!(Multiply::new(3)),
    );

    let mut pipeline = sequence!(pipeline1, pipeline2);

    pipeline.send(&String::from("12345")).unwrap();
    pipeline.tick().unwrap();
    assert_eq!(pipeline.try_recv().unwrap(), Some(30));
}

#[test]
fn test_blocking() {
    let mut pipeline = sequence!(
        step!(StringLength::new()),
        step!(Chunk::new(3)),
        step!(Sum::new()),
    );

    pipeline.send(&String::from("12345")).unwrap();
    pipeline.send(&String::from("123")).unwrap();
    pipeline.send(&String::from("1234567")).unwrap();

    pipeline.tick().unwrap();

    assert_eq!(pipeline.try_recv().unwrap(), Some(15));
}
