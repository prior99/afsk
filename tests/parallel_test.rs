use radio::algorithm::{Add, Algorithm, ReturnFn, Chunk, Multiply, StringLength, Stringify, Sum};
use radio::error::Error;
use radio::{parallel, sequence, step};
use radio::pipeline::Node;

struct CombineTuple {}

impl Algorithm<(u32, String, u32), u32> for CombineTuple {
    fn algorithm<'a>(
        &mut self,
        input: (u32, String, u32),
        next: &mut ReturnFn<u32>,
    ) -> Result<(), Error> {
        next(input.0 + input.1.len() as u32 + input.2)
    }
}

impl CombineTuple {
    pub fn new() -> Self {
        Self {}
    }
}

#[test]
fn test_parallel() {
    let mut pipeline = parallel!(
        step!(Add::new(3)),
        step!(Stringify::new()),
        step!(Add::new(5))
    );

    pipeline.send(&10).unwrap();
    pipeline.tick().unwrap();
    assert_eq!(pipeline.try_recv().unwrap(), Some((13, "10".into(), 15)));
}

#[test]
fn test_parallel_and_sequence() {
    let mut pipeline = sequence!(
        step!(StringLength::new()),
        parallel!(
            step!(Add::new(3)),
            step!(Stringify::new()),
            step!(Add::new(5))
        ),
        step!(CombineTuple::new()),
        step!(Multiply::new(3)),
    );

    pipeline.execute(&String::from("12345")).unwrap();
    pipeline.tick().unwrap();
    assert_eq!(pipeline.try_recv().unwrap(), Some(57));
}

#[test]
fn test_imbalanced_chunking() {
    let mut pipeline = sequence!(
        step!(StringLength::new()),
        parallel!(
            sequence!(step!(Chunk::new(3)), step!(Sum::new())),
            step!(Stringify::new()),
            sequence!(step!(Chunk::new(5)), step!(Sum::new())),
        ),
        step!(CombineTuple::new()),
        step!(Multiply::new(3)),
    );

    for _i in 0..15 {
        pipeline.send(&String::from("four")).unwrap();
    }
    pipeline.tick().unwrap();

    assert_eq!(pipeline.try_recv().unwrap(), Some(99));
}
