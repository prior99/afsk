use std::{fs::File, path::Path};

use radio::algorithm::Chunk;
use radio::pipelines::{ax25, bell_202};
use radio::wav_utils::Wav;
use radio::{sequence, step};
use wav::BitDepth;

use radio::ax25::{
    Address, Control, Direction, Information, ProtocolId, Repeater, Station, UnnumberedControl,
    AX25,
};
use radio::pipeline::{Node, Pipeline};

fn input_file(filename: &str) -> (BitDepth, Pipeline<BitDepth, AX25>) {
    let path = Path::new(env!("CARGO_MANIFEST_DIR"))
        .join("samples/fixtures")
        .join(filename);
    let mut file = File::open(path).unwrap();
    let (header, data) = wav::read(&mut file).unwrap();
    (
        data,
        sequence!(
            step!(Wav::new()),
            step!(Chunk::new(512)),
            bell_202(header.sampling_rate).unwrap(),
            ax25().unwrap(),
        ),
    )
}

#[test]
fn test_from_wikimedia() {
    let (data, mut pipeline) = input_file("from_wikimedia.wav");
    pipeline.execute(&data).unwrap();
    assert_eq!(
        pipeline.try_recv().unwrap(),
        Some(AX25 {
            address: Address {
                source: Station {
                    callsign: "S57LN".into(),
                    ssid: 0
                },
                destination: Station {
                    callsign: "APRS".into(),
                    ssid: 0
                },
                direction: Direction::Command,
                repeaters: vec![]
            },
            control: Control::Unnumbered {
                poll_final: false,
                control: UnnumberedControl::UnnumberedInformation
            },
            pid: Some(ProtocolId::NothingImplemented),
            info: Information {
                bytes: "=4603.63N/01431.26E-Op. Andrej".into()
            }
        })
    );
    assert_eq!(pipeline.try_recv().unwrap(), None);
}

#[test]
fn test_noisy_11520() {
    let (data, mut pipeline) = input_file("noisy_11250.wav");
    pipeline.execute(&data).unwrap();
    assert_eq!(
        pipeline.try_recv().unwrap(),
        Some(AX25 {
            address: Address {
                source: Station {
                    callsign: "DL0PBS".into(),
                    ssid: 5
                },
                destination: Station {
                    callsign: "APMI03".into(),
                    ssid: 0
                },
                direction: Direction::Response,
                repeaters: vec![
                    Repeater {
                        station: Station {
                            callsign: "DB0OL".into(),
                            ssid: 10
                        },
                        has_repeated: false
                    },
                    Repeater {
                        station: Station {
                            callsign: "DB0WOT".into(),
                            ssid: 0
                        },
                        has_repeated: false
                    }
                ]
            },
            control: Control::Unnumbered {
                poll_final: false,
                control: UnnumberedControl::UnnumberedInformation
            },
            pid: Some(ProtocolId::NothingImplemented),
            info: Information {
                bytes: vec![
                    84, 35, 48, 55, 53, 44, 49, 54, 53, 44, 49, 51, 57, 44, 48, 48, 48, 44, 48, 48,
                    48, 44, 49, 54, 50, 44, 48, 48, 48, 48, 48, 48, 48, 48
                ]
            }
        })
    );
    assert_eq!(
        pipeline.try_recv().unwrap(),
        Some(AX25 {
            address: Address {
                source: Station {
                    callsign: "DB0ET".into(),
                    ssid: 0
                },
                destination: Station {
                    callsign: "U3RTS5".into(),
                    ssid: 0
                },
                direction: Direction::Response,
                repeaters: vec![
                    Repeater {
                        station: Station {
                            callsign: "DB0OL".into(),
                            ssid: 10
                        },
                        has_repeated: false
                    },
                    Repeater {
                        station: Station {
                            callsign: "DB0WOT".into(),
                            ssid: 0
                        },
                        has_repeated: false
                    }
                ]
            },
            control: Control::Unnumbered {
                poll_final: false,
                control: UnnumberedControl::UnnumberedInformation
            },
            pid: Some(ProtocolId::NothingImplemented),
            info: Information {
                bytes: [
                    39, 125, 88, 122, 108, 32, 28, 114, 47, 34, 52, 88, 125, 52, 51, 56, 46, 57,
                    50, 53, 77, 72, 122, 32, 70, 77, 32, 82, 101, 112, 101, 97, 116, 101, 114, 32,
                    76, 72, 32, 67, 97, 109, 112, 101, 110
                ]
                .into()
            }
        })
    );
    assert_eq!(
        pipeline.try_recv().unwrap(),
        Some(AX25 {
            address: Address {
                source: Station {
                    callsign: "DB0FS".into(),
                    ssid: 10
                },
                destination: Station {
                    callsign: "APMI06".into(),
                    ssid: 0
                },
                direction: Direction::Unknown,
                repeaters: vec![]
            },
            control: Control::Unnumbered {
                poll_final: false,
                control: UnnumberedControl::UnnumberedInformation
            },
            pid: Some(ProtocolId::NothingImplemented),
            info: Information {
                bytes: [
                    64, 49, 48, 50, 51, 52, 53, 122, 53, 51, 51, 53, 46, 56, 54, 78, 73, 48, 48,
                    57, 53, 54, 46, 56, 55, 69, 38, 73, 71, 97, 116, 101, 32, 38, 32, 68, 105, 103,
                    105, 32, 104, 116, 116, 112, 58, 47, 47, 97, 102, 117, 110, 100, 114, 46, 100,
                    101, 32, 32, 85, 61, 49, 50, 46, 50, 86, 44, 84, 61, 50, 49, 46, 51, 67
                ]
                .into()
            }
        })
    );
    assert_eq!(
        pipeline.try_recv().unwrap(),
        Some(AX25 {
            address: Address {
                source: Station {
                    callsign: "DB0FS".into(),
                    ssid: 10
                },
                destination: Station {
                    callsign: "APMI06".into(),
                    ssid: 0
                },
                direction: Direction::Unknown,
                repeaters: vec![Repeater {
                    station: Station {
                        callsign: "DL5GNH".into(),
                        ssid: 1
                    },
                    has_repeated: false
                }]
            },
            control: Control::Unnumbered {
                poll_final: false,
                control: UnnumberedControl::UnnumberedInformation
            },
            pid: Some(ProtocolId::NothingImplemented),
            info: Information {
                bytes: [
                    64, 49, 48, 50, 51, 52, 53, 122, 53, 51, 51, 53, 46, 56, 54, 78, 73, 48, 48,
                    57, 53, 54, 46, 56, 55, 69, 38, 73, 71, 97, 116, 101, 32, 38, 32, 68, 105, 103,
                    105, 32, 104, 116, 116, 112, 58, 47, 47, 97, 102, 117, 110, 100, 114, 46, 100,
                    101, 32, 32, 85, 61, 49, 50, 46, 50, 86, 44, 84, 61, 50, 49, 46, 51, 67
                ]
                .into()
            }
        })
    );
    assert_eq!(pipeline.try_recv().unwrap(), None);
}
